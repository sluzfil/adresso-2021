# Evaluation of ASR results


## Converting Google ASR JSON files to CHAT format

ASR transcripts were generated from the enhanced audio files through
the Google ASR API. The JSON output was then converted into CHAT files
by `../util/googleasr2chat.py`:

```sh
## convert Fasih's .txt files 
for f in *.txt ; do mv $f $(echo "$f" | sed s/-trans.txt//)-asr.json     ; done 
## conver all .json to .cha
for f in *.json ; do googleasr2chat.py $f   ; done 
```

The ASR transcripts are stored in the relevantly named directories:

```
../data
   ├── test
   │   ├── asr
   │   └── ....
   └── train
       ├── asr
       │   ├── ad
       │   └── cn
       |   ...
       └── ...
```




## Software:

- ASR software (Google ASR API used in this case)

- [util/googleasr2chat.py](../util/googleasr2chat.py): Convert Google
  ASR JSON files into basic CHAT files

- [util/chat2stm.pl](../util/chat2stm.pl): convert CHAT file to
  segment time mark (STM) format used by NIST's sclite tool which
  scores speech recognition system output

- [util/getasrstats.pl](../util/getasrstats.pl): extract WER stats
  from NIST sclite SUM style output (-o sum)
- NIST's [SCTK](https://github.com/usnistgov/SCTK): NIST Scoring
  Toolkit 

