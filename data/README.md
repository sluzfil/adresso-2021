# [ADReSSo IS-2021](https://edin.ac/3p1cyaI) data 

The file `adressods.csv` contains a full summary of the dataset. 

This directory contains the full ADReSSo dataset, in the following
hierarchy:

```
.
├── ChialFlahive -> ../../data/ChialFlahive/
├── Olness -> ../../data/Olness/
├── Pitt -> ../../data/Pitt/
├── RHDBank -> ../../data/RHDBank/
├── diagnosis
│   ├── test
│   │   ├── asr
│   │   ├── audio
│   │   └── transcripts
│   └── train
│       ├── asr
│       │   ├── ad
│       │   └── cn
│       ├── audio
│       │   ├── ad
│       │   └── cn
│       └── transcripts
│           ├── ad
│           └── cn
├── distrib
│   └── ADReSSo21
│       ├── diagnosis
│       │   ├── test
│       │   │   ├── audio
│       │   │   └── segmentation
│       │   ├── test-dist
│       │   └── train
│       │       ├── audio
│       │       │   ├── ad
│       │       │   └── cn
│       │       └── segmentation
│       │           ├── ad
│       │           └── cn
│       └── progression
│           ├── test
│           │   ├── audio
│           │   └── segmentation
│           ├── test-dist
│           └── train
│               ├── audio
│               │   ├── decline
│               │   └── no_decline
│               └── segmentation
│                   ├── decline
│                   └── no_decline
├── progression
│   ├── test
│   │   ├── audio
│   │   └── transcripts
│   └── train
│       ├── audio
│       │   ├── decline
│       │   └── no_decline
│       └── transcripts
│           ├── decline
│           └── no_decline
├── test
│   ├── audio
│   └── transcripts
└── train
    ├── audio
    │   ├── ad
    │   └── cn
    └── transcripts
        ├── ad
        └── cn
```

The symlinks can be ignored. The distrib directory contains the README
files and directory structure for the dataset distribution. 

The `diagnosis` directory contain the data for the diagnosis and MMSE
score prediction tasks, while `progression` contains the disease
progression (cognitive decline) prediction task.

This directory also contains files 

- `README-train.md`: with a description of the training set, to be
  released together with subtrees:

```
  ├── diagnosis
  │   └── train
  │       ├── audio
  │           ├── ad
  │           └── cn
  └── progression
    └── train
        ├── audio
            ├── decline
            └── no_decline
```

- `README-test.md`: with a description of the test set (to be
  withheld). This refers to subtrees:
  
  ```
  ├── diagnosis
  │   ├── test
  │       ├── audio
  │       └── transcripts
  └── progression
      ├── test
          ├── audio
          └── transcripts
```

## ASR transcriptions

ASR transcripts were generated from the enhanced audio files through
the Google ASR API. The JSON output was then converted into CHAT files
by `../util/googleasr2chat.py`:

```sh
## convert Fasih's .txt files 
for f in *.txt ; do mv $f $(echo "$f" | sed s/-trans.txt//)-asr.json     ; done 
## conver all .json to .cha
for f in *.json ; do googleasr2chat.py $f   ; done 
```

The ASR transcripts are stored in the relevantly named directories:

```
   ├── test
   │   ├── asr
   │   └── ....
   └── train
       ├── asr
       │   ├── ad
       │   └── cn
       |   ...
       └── ...
```
