# This directory contains the training data for the [IS2021 ADReSSo Challenge](https://edin.ac/3p1cyaI).

The data are in the following directories structure and files:

```
progression
└── train
    ├── audio
        ├── decline
        └── no_decline
```

This directory contains the data for the disease progression
(cognitive decline) prediction task. 

The data consist of enhanced, volume normalised audio (.wav) files and
utterance segmentation files (diarisation), in CSV format. The latter
are for those who choose to do the segmented prediction sub-task. The
segmented prediction and speech-only sub-tasks will be assessed
separately.



