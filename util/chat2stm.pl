#!/usr/bin/perl
# 
# cha2stm.PL --- convert CHAT file to segment time mark (STM) format
# used by NIST's sclite tool which scores speech recognition system
# output

# Author:  <s.luz@ed.ac.uk>
# Created: 22 Mar 2021
# $Revision$
# Keywords:


#     Permission  to  use,  copy, and distribute is hereby granted,
#     providing that the above copyright notice and this permission
#     appear in all copies and in supporting documentation.

# Commentary:

# Change log:
#

# Code:
my $rcsid =   "\$Id:  $>";
my $version = substr("\$Revision$>",11,-length($0));

BEGIN{ 
    $0 =~ /(.*)\/[^\/]/;
    $pgd = $1;
    push (@INC,("./",
	       "$pgd/",
	       "$pgd/Lib/"
		));
		$me   = $0;
		$me =~ s/.*\///;
}
use Getopt::Std;


sub usage {
    die<<"END";

  $me version $version
  convert CHAT file to segment time mark (STM) format used by NIST's
  sclite tool which scores speech recognition system usage: $me [-h]
  chafile

  Options:          
      -h	  Help   just display this message and quit.
      -f          chafile name of a CHAT file to convert into STM format
      -t          Output text file (one sentence per line) instead of STM
END
}

getopts('htf:');
usage
    if ( $opt_h );

my $media = $1
    if $opt_f =~ /(.+)\..+/;
my $stmfname = $media.'.stm';
$stmfname = $media.'.txt'
    if $opt_t;


my $content = '';
print STDERR "Converting  $opt_f ->  $stmfname\n";
open(FI, $opt_f) or die ("error opening $opt_f: $1");

my $stm_F = '';  ## Media file name
my $stm_C = 'A'; ## The waveform channel. The text of the waveform
                 ## channel is not restricted by sclite. The text can
                 ## be any text string without witespace so long as
                 ## the matching string is found in both the reference
                 ## and hypothesis input files.
my $stm_S = ''; # The speaker id, no restrictions apply to this name.
my $stm_transcript = ''; ## The transcript can take on two forms: 1) a
                         ## whitespace separated # list of words, or
                         ## 2) the string #
                         ## "IGNORE_TIME_SEGMENT_IN_SCORING".
my $stm_BT = 0; # The begin time (seconds) of the segment.
my $stm_ET = 0; # The end time (seconds) of the segment.


while(<FI>)
{
    $stm_F = $2.'.wav'
        if /(\@Media:[\t ]+)(.+?),/;
    if ( /\*(PAR|INV):[\t ]+(.+)/ ) {
        $stm_C = 'A';
        $stm_S = $1; 
        my $tmp = $2;
        while(<FI>){
            if ( /^\t+(.+)/ ){
                $tmp .= " $1";
            }
            else {
                last;
            }
        }
        
        if ($tmp =~ /(.+?)([0-9]+)_([0-9]+)/){ 
            $stm_BT = $2/1000; # The begin time (seconds) of the segment.
            $stm_ET = $3/1000; # The end time (seconds) of the segment.
            $stm_transcript = $1;
            #print "---> $stm_transcript";
        }
        else {
            print STDERR "WARNING: No time tags found in $tmp\n";
            $stm_transcript = $tmp;
        }
        $stm_transcript = lc $stm_transcript;
        $stm_transcript =~ s/\((.+?)\)/$1/g;
        $stm_transcript =~ s/<(.+?)>/$1/g;
        $stm_transcript =~ s/[<“”‡\/]+|&[a-z]+|0[a-z]+|&=[a-z:]+|\[.*?\]|[\.\?\!;,\+\-]+//g;
#|0[a-z]+|&=[a-z:]+)//g;
#            print "===> $stm_transcript";

        $content .= $opt_t? "$stm_transcript\n" :
            "$stm_F $stm_C $stm_S $stm_BT $stm_ET $stm_transcript\n";
    }
}
    
close FI;

open(FO, ">$stmfname") or die ("error opening $stmfname: $1");

print FO $content;

close FO;


# end 
