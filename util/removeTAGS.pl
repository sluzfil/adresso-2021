#!/usr/bin/perl
# 
# REPLACEIDS.PL --- 

# Author:  <s.luz@ed.ac.uk>
# Created: 22 Jan 2020
# $Revision$
# Keywords:


#     Permission  to  use,  copy, and distribute is hereby granted,
#     providing that the above copyright notice and this permission
#     appear in all copies and in supporting documentation.

# Commentary:

# Change log:
#

# Code:
my $rcsid =   "\$Id:  $>";
my $version = substr("\$Revision$>",11,-length($0));

BEGIN{ 
    $0 =~ /(.*)\/[^\/]/;
    $pgd = $1;
    push (@INC,("./",
	       "$pgd/",
	       "$pgd/Lib/"
		));
		$me   = $0;
		$me =~ s/.*\///;
}
use Getopt::Std;


sub usage {
    die<<"END";

  $me version $version

  usage: $me  [-h] 

  Options:          
      -h	  Help   just display this message and quit.

END
}

getopts('hf:');
usage
    if ( $opt_h );
my $infname = "$opt_f.old";
rename($opt_f, $infname);

my $media = $1
    if $opt_f =~ /(.+)\..+/;

my $content = '';
open(FI, $infname) or die ("error opening $infname: $1");
my $ct = 0;
while(<FI>)
{
    print STDOUT "$infname: Replace $2 --> $media\n "
        if s/(@Media:[\t ]+)([-0-9]+),/$1$media,/;
    print STDOUT "$infname: Removed class: $2, MMSE: $3 ($ct)\n"
        if s/(@ID:.+)\|(.+?)\|\s*\|Participant\|(.+?)\|/$1|_X_||Participant|_Y_|/;
    $content .= $_;
    }
close FI;

open(FO, ">$opt_f") or die ("error opening $opt_f: $1");

print FO $content;

close FO;


# end 
