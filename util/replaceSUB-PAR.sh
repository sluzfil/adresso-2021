## to run interactively on the command line

## replace start of line tags
for f in *.cha ; do sed 's/^\*SUB:/\*PAR:/' $f > $f.new ; done

### replace SUB in text and notes
for f in *.cha ; do sed 's/\(\W\)SUB\(\W\)/\1PAR\2/g'  $f > $f.new ; done

## check the results with
cat *.new|less

## if results are okay, replace .new for original files
for f in *.cha.new ; do mv $f `echo $f |sed 's/\.new//'` ; done
